// my_math_test.c
#include <stdio.h>
#include <gtest/gtest.h>
#include "my_math.h"

TEST(Math_Test, checkPrimeNumber2) {
    EXPECT_EQ(0, checkPrimeNumber(8));
}

TEST(Math_Test, checkPrimeNumber4) {
    EXPECT_EQ(1, checkPrimeNumber(7));
}